#include <iostream>

using namespace std;

int main() {

	//For Loops - easily specify the amount of times to iterate
	for (int i = 0; i < 10; i++) //create a variable that increases every iteration and loops as long as it's less than 10
	{
		cout << i << endl; //print which number it's on
	}
	cout << "Loop done running" << endl;

	//For loops are very useful for looping through arrays
	//simple for loop that lets you add values to an array
	const int ARRAY_SIZE = 5;
	string names[ARRAY_SIZE] = { " " };
	cout << "I need five names! Please help me out!" << endl;
	for (int i = 0; i < ARRAY_SIZE; i++)
	{
		cout << "Give me a name: ";
		cin >> names[i];
	}
	cout << "Thanks! Let me show you all of the numbers now" << endl;
	for (int i = 0; i < ARRAY_SIZE; i++)
	{
		cout << names[i] << endl;
	}

	//Range-Based for loops
	int numbers[] = { 1, 13, 13, 4, 2, 4124 };
	for (int element : numbers) //you can use auto instead of int
	{
		cout << "Elements of the array: " << element << endl;
	}

	//Nested for loops
	for (int a = 0; a < 5; a++)
	{
		for (int b = 0; b < 3; b++)
		{
			cout << a << ", " << b << endl;
		}
	}

	//use a nested for loop to display a multidimensional array
	int cats[4][3] = {
		{5, 2, 4},
		{2, 14, 1},
		{45, 2, 56},
		{609, 324, 1}
	};

	for (int row = 0; row < 4; row++)
	{
		for (int column = 0; column < 3; column++)
		{
			cout << "Element[" << row << "][" << column << "]: " << cats[row][column] << endl;
		}
	}

	return 0;
}

