#include <iostream>

using namespace std;

//This function dynamically allocates a string but forgets to free up the memory before ending the function
void printName() {

    string* grandpaName = new string("Grandpa Jenkins");

    cout << "My Grandpa's name is " << *grandpaName << endl;
}

int main() {


    /////////////////////////////////////////////////
    // Dynamic Memory Allocation
    // What does it mean to dynamically allocate memory? This means to create memory on the "Heap"
    // rather than the "Stack". The heap and stack are 2 of the sections of memory for applications.
    // Why allocate memory on the Heap rather than the Stack?

    // The Stack can only be a certain size of memory, sometimes around 1 MB. If in your program
    // you allocate more memory than can fit inside the stack, you will get a "Stack Overflow" error.
    // This is also the idea behind declaring arrays with constant values, since knowing the size of
    // the array ahead of time can avoid these types of issues.

    // The Heap can be much larger than the Stack and can also change in size with no problems(Unless you go
    // beyond the limitations of the system itself). So if you find yourself in a situation
    // in which you might be allocating a lot of memory during the life of your program,
    // dynamically allocating it outside of the Stack and onto the Heap is the way to go.
    // Same thing for if you find yourself not knowing the size of arrays and other data until the program
    // is already running

    //There is a lot more to memory allocation and sections of memory, so check out these resources if interested
    // https://www.youtube.com/watch?v=CSVRA4_xOkw
    // https://www.youtube.com/watch?v=_8-ht2AKyH4
    // https://www.w3schools.in/cplusplus-tutorial/dynamic-memory-allocation/

    //How to dynamically allocate variables
    // in C++, we can use the "new" keyword to dynamically allocate something onto the heap
    int age = 18; //A simple variable that will be allocated on the stack
    int* heapAge = new int; //Dynamically allocate an integer on the heap and return the address.

    // the reason we use a pointer above when dynamically allocating is because we otherwise have no way
    // of keeping track where that new integer was created and accessing it
    //From there, we can do whatever we normally would do with a pointer to an integer
    *heapAge = 99; //Set the value of that integer on the heap to 99
    cout << *heapAge << endl; //Print the value of the integer

    //What would happen if we tried printing out the value before we set it? For example:
    double* score = new double; //Allocating 8 bytes for a double
    //We aren't providing an initial value when allocating so what happens if we print it?
    cout << "Here is the current score: " << *score << endl;
    //We print out a garbage value. Whatever may happen to be stored at that memory address will be printed
    *score = 100.0;
    cout << "Here is the current score: " << *score << endl;
    //now we get a legitimate value from that memory address

    //Is there an easier way to initialize the value as soon as we allocate it? Yes
    string* name = new string("Mr. Jenkins");

    ////////////////////////////////////////////////////////////
    // De-allocating Dynamic Memory
    // When you are done using the dynamically allocated memory, it is good to de-allocate it
    // so that you can open space for new memory in the future.
    // If you were to never deallocate anything, then eventually the Heap may fill up and you have no space left
    // The term for memory that has never been freed because you forgot or did it incorrectly is a "memory leak"

    //free up allocated memory with the delete keyword
    int* size = new int(140);
    cout << "The size of my brain is " << *size << " inches" << endl;
    //Now that I am done using this memory, I can free it up for the program
    delete size; //deallocated the 4 bytes the pointer is pointing to

    //A particularly sticky situation for freeing memory is introduced when you use it in functions
    printName();
    //Every time we call this function, we dynamically allocate a string but don't free it up.
    //Since we allocated that string in the scope of the function, there is no way to get
    //that pointer back and free it up. It's lost forever and we have introduced a memory leak.
    //
    // So in short, make sure to free up dynamic memory before a function ends.
    // OR return the pointer to the dynamic memory to be free'd up somewhere else

    ////////////////////////////////////////////////////////////////
    // Dangling Pointers
    // If you have a pointer that points to a dynamically allocated chunk of memory
    // and free it up, you should immediately reassign the pointer to a new valid
    // memory address or set it to nullptr. Otherwise, you have a dangling pointer
    // A dangling pointer is a pointer that points to memory that is no longer accessible
    // since it has been freed up.
    int* thing = new int;
    delete thing;
    *thing = 56; //bad boy
    //do this instead:
    thing = nullptr;

    ////////////////////////////////////////////////////////////////
    //Something to remember. 
    // Although the pointer may point to some memory on the heap,
    // the pointer variable itself exists on the stack

    return 0;
}