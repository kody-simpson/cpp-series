#include <iostream>
#include <string> //Sometimes to be able to use strings, this must be included manually

using namespace std;

int main() {

	//Strings - text that can be stored
	string name = "Randy";
	cout << name << endl;
	//We can change the value of course
	name = "Jerry";
	cout << name << endl;

	//Output multiple strings at once
	cout << "Here is my name: " << name << endl;
	string sentence = "Here is my name: ";
	cout << sentence << name << endl;

	//Adding strings - Concatenation
	string name2 = "bob";
	string two_names = name + name2;
	cout << two_names << endl;

	string book = "Cant Hurt Me";
	string finalString = "My favorite book is: " + book;
	cout << finalString << endl;

	return 0;
}
