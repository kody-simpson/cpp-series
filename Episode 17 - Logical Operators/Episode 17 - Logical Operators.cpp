#include <iostream>

using namespace std;

int main() {


	//Logical Operators - make complex conditions
	//! -  NOT operator
	//&& - AND operator
	//|| - OR operator

	int var1 = 1000;
	if (!(var1 == 100)) //Opposite of the condition
	{
		cout << "Variable is not equal to 100" << endl;
	}

	if (var1 == 1000 && var1 > 10) //Both conditions have to be true
	{
		cout << "Both conditions were satisfied" << endl;
	}

	string name;
	cout << "Give us a name: " << endl;
	cin >> name;
	if (name == "Bobby" || name == "Jerry") //Only one condition has to be true
	{
		cout << "You chose one of the special names" << endl;
	}

	//Even more complex
	//Ask for an age too
	int age;
	cout << "Give us an age too: ";
	cin >> age;
	if ((name == "Bobby" || name == "Jerry") && age > 18)
	{
		cout << "You met the requirements" << endl;
	}
	else {
		cout << "Wrong info!" << endl;
	}


	return 0;
}
