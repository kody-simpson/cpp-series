#include <iostream>

using namespace std;

int main()
{

	/////////////////////////////////////////////
	///More Enums
	///
	///Declaring variables with the enum type
	///The basic way of doing it
	enum Color {Red, Blue, Pink, Yellow, Purple};
	Color color1 = Blue;

	//We can actually declare our enum and enum variables together
	enum Food {Pancake, Burger, Taco, Croissant} favoriteFood = Croissant, anotherFood;
	cout << favoriteFood << endl;

	////////////////////////////////////////////
	///Strongly Typed Enumerations - AKA enum class
	///Strongly types enumerations allows you to have the same enumerator
	///in two different enumerations
	///

	//Does not work because they have the same enumerator:
	// enum People { Bob, Billy, Joe, Randy, Cleetus };
	// enum Dork { Ralph, Joe, Sandy, Brock };

	//You can use the same enumerator in 2 different enums if you
	//make them enum classes or Strongly Typed Enumerations
	enum class People {Bob, Billy, Joe, Randy, Cleetus};
	enum class Dork {Ralph, Joe, Sandy, Brock};

	//You have to specify which enum you are using with :: (scope resolution operator)
	People person = People::Bob;

	if (person < People::Randy)
	{
		cout << "Pickle" << endl;
	}

	//Another thing about strongly types enums, you have to cast it
	//just to get the underlying integer constant
	cout << static_cast<int>(person) << endl;
	
}
