#include <iostream>

using namespace std;

int main() {

	//While Loop - repeat code while a condition is true
	int num = 1000;
	while (num > 100) //if this is true, the block of code is run. Stops when it becomes false
	{
		cout << "The number is bigger than 100: " << num << endl;
		num--; //subtract one from the number
	}

	//another example
	char again = 'y'; //initialized as yes so they play the game at least once
	while (again == 'y') //check to see if they changed the value or not
	{
		cout << "*plays a game*" << endl;
		cout << "Do you want to play the game again? Type y or n " << endl;
		cin >> again; //ask if they want to play again
	}

	//Do-While loops - Like a while loop except that it tests AFTER running, not before
	string runAgain;
	do {
		int num1, num2;
		cout << "Give me two numbers to add: ";
		cin >> num1 >> num2;
		cout << "Numbers added: " << (num1 + num2) << endl;
		cout << "Would you like to run again? Type yes or no " << endl;
		cin >> runAgain;
	} while (runAgain == "yes"); //only repeats if they types yes


	return 0;
}
