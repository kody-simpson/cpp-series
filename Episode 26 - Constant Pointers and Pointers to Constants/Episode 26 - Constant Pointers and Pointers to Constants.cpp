#include <iostream>

using namespace std;

int main() {

    //////////////////////////////////////////////////////////////
    //Pointers to Constants
    const int bob = 45; //a variable that does not change
    //int* pointer = &bob; You cannot do this!!! It must point to a constant integer not a integer
    const int* pointerToBob = &bob; //Valid

    cout << "Value of bob: " << *pointerToBob << endl;
    //bob = 23; //cannot be done, it's a const
    //&pointerToBob = 45; //cannot be done, it's a const integer at that memory location

    //Even though the value at that memory address cannot be changed,
    //the pointer's address can be changed.
    //The pointer itself is not a constant. Only the value being pointed to
    const int ricky = 99;
    pointerToBob = &ricky; //Pointer now points to another const int. Ricky :)
    cout << "Value of bob: " << *pointerToBob << endl;

    //Below you see me making a pointer to a constant integer.
    //thing is not a const but it's still allowed
    int thing = 2;
    const int* thingPointer = &thing;

    /////////////////////////////////////////////////////////////
    //Constant Pointers
    double score = 100.0;
    double* const ptrScore = &score; //A pointer whose memory address is constant
    cout << "Value at ptrScore: " << *ptrScore << endl;
    //ptrScore = &bob; Memory address of the pointer cannot change

    //The value stored AT that memory address still can change.
    *ptrScore = 200.0;
    cout << "Value at ptrScore: " << *ptrScore << endl;

    /////////////////////////////////////////////////////////////
    //Constant Pointers to Constants
    //Combining the two above concepts, the below pointer is a pointer
    //whose memory address cannot be changed and the value being pointed to
    const string name = "Kody Simpson";
    const string* const stringPtr = &name;

    //Cannot be done
    //&stringPtr = 34;
    const string name2 = "Commander";
    //stringPtr = &name2;

    //////////////////////////////////////////////////////////////

    return 0;
}