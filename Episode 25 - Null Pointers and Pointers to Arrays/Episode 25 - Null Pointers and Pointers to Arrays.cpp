#include <iostream>

using namespace std;

const int NUM_SCORES = 5;

int main() {

    //---------------------------------------------------------------------
    //More on declaring pointers
    //If you make a new pointer without immediately initializing it with a memory address
    //, give it an initial value of 0, NULL, or nullptr. nullptr is best practice
    int* ptr; //Don't do this because it will be initialized with a "garbage" memory address

    double* doublePtr = nullptr; //Will effectively set the memory address to 0, so that it points to nothing
    //double* doublePtr = 0;
    //double* doublePtr = NULL;
    cout << doublePtr << endl;

    //---------------------------------------------------------------------
    //Pointers to Arrays
    int numbers[] = { 1, 4, 23, 6, 2, 562 };

    //Getting the address of an array
    //Since arrays are a group of variables, the memory address of the array
    //is the memory address of the first variable in the array
    cout << "Address of numbers array: " << &numbers << endl; //Same as any variable
    cout << "Address of numbers array: " << numbers << endl; //With arrays you can actually just use the name itself as an address

    //Create a pointer that points to an array of doubles
    double scores[NUM_SCORES] = { 100.0, 56.7, 14.5, 97.5, 75.0 }; //Allocated 5 consecutive slots of memory for these doubles; 8 * 5 = 40 bytes total.
    double* scoresPointer = scores; //Set the memory address of the first element of the array equal to a pointer to a double
    //We as the programmer know that this pointer points to an array, but it's still just a regular pointer to a double.
    //Therefore, it's smart to keep track of the size of the array in a variable. You will see why

    //Here is why: What if you want to access the elements after the first array element from the pointer? hmm....
    //Print out the first value of the array
    cout << "First double score of the array: " << *scoresPointer << endl; //Don't forget to dereference it

    //How to go beyond the first element if the pointer only holds the address of the first element
    //If the array allocated the elements consecutively(next to each other) then we just need to find the address of the next 8 bytes(because double)
    cout << "Address of the first element: " << scoresPointer << endl;
    cout << "Address of the second element: " << scoresPointer + 1 << endl; //Does not add 1 byte. Adds EIGHT bytes to the address. C++ recognizes that this is a pointer to a double
    cout << "Address of the third element: " << scoresPointer + 2 << endl; //Adds 18 bytes, to get the address at that location

    //Now, to get the values, dereference those memory addresses
    cout << "Value of the first element: " << *scoresPointer << endl;
    cout << "Value of the second element: " << *(scoresPointer + 1) << endl; //You need to have parentheses because of order of operations
    cout << "Value of the third element: " << *(scoresPointer + 2) << endl;

    //C++ offers an easier syntax for accessing elements of a pointer you know points to an array
    cout << "Value of the fifth element: " << scoresPointer[4] << endl; //Same as you would access it normally

    //BE CAREFUL YOUNG BLOOD
    //Going beyond the size of the array is dangerous, you are accessing memory that you have no idea what the values are
    //and changing them could also be disastrous!
    //So, again, keep careful track of the array size and don't go beyond it
    for (int i = 0; i < NUM_SCORES; i++) {
        cout << "Value of element(" << i << "): " << scoresPointer[i] << endl;
        cout << "Address of element(" << i << "): " << &scoresPointer[i] << endl;
    }

    //---------------------------------------------------------------------

    //Comparing Pointers
    //We already saw we can do some arithmatic on pointers, which will effect the memory address
    //but we can also compare them with < > <= >= == !=

    string names[] = { "Kody Simpson", "Ronald Reagan", "Richard Nixon", "Donald Trump", "Abraham Lincoln" };
    string* pointerToStrings = nullptr;
    pointerToStrings = names;
    if (&pointerToStrings[0] > & pointerToStrings[1]) { //False because memory addresses that come after are deemed "larger"
        cout << "BEEP BEEP" << endl;
    }
    else {
        cout << "Told ya" << endl;
    }

    if (&pointerToStrings[2] == &pointerToStrings[2]) { //true, it's the same memory address
        cout << "Same memory address" << endl;
    }

    return 0;
}