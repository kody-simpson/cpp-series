#include <iostream>

using namespace std;

//Our class Dog, which will be used to make Dog objects
class Dog
{
public:
	//member variables(attributes)
	string name;
	int age;
	int numPaws;
	double health;

	//You can define the function in the class definition
	//but it's common practice to do it outside
	void bark()
	{
		cout << "Woof woof!" << endl;
	}
	//prototype for the function defined outside of the class
	void growl();
};

//This is how you can provide the implementation for a function
//outside of the class definition. You must use the scope resolution operator,
//as seen with enumerations previously.
void Dog::growl()
{
	cout << "Grr...." << endl;
}


int main()
{

	//Create a dog (an instance of the Dog class)
	Dog fluffy;

	//access and set the attributes of the dog
	fluffy.name = "Fluffy";
	fluffy.age = 3;
	fluffy.numPaws = 4;
	fluffy.health = 100.0;

	cout << "Health of fluffy: " << fluffy.health << endl;

	//Create a dog and provide an initialization list
	Dog dog2 = { "Henry", 6, 2, 20.0 };

	//Calling object functions
	dog2.bark();
	fluffy.growl();

	
}
