#include <iostream>
#include <iomanip>

using namespace std;

int main() {

	float floating_number = 5.5213131313;
	cout << setprecision(20) << floating_number << endl;

	double double_number = 1.525214571145611;
	cout << setprecision(20) << double_number << endl;

	long double longDouble = 123.11142141241241412412999;
	cout << setprecision(50) << longDouble << endl;

	return 0;
}

