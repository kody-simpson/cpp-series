#include <iostream> //Include the iostream header file in our file

using namespace std; //Automatically use the standard library namespace

//Every program needs a main function, like the one below
//It's referred as the "entry point" of the program because
//this is where execution starts
int main() {

	//An output statement. We pass a string literal to the cout output stream
	cout << "Hello World!"; 

	return 0; //0 or nothing means the program ran successfully. a Non-zero value means failure.
}

