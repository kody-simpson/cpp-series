#include <iostream>

using namespace std;

int main() {

	//Number Datatypes (http://www.cplusplus.com/reference/climits/)
	//int, long, short, long long, (unsigned types)
	//They all hold whole numbers, but have different limits in terms of the size

	int number = 2147483647; //officially between -32767 and 32767(allows -2147483647 and 2147483647)
	unsigned int number2 = 4000; //between 0 and 65535

	//Big Numbers
	long int number3 = 2147483649; //between -2147483647 and 2147483647
	unsigned long int number4 = 4294967295; //between 0 and 4294967295

	//Smaller Numbers
	short int number5 = 1233; //between -32767 and 32767
	unsigned short int number6 = 2131; //between 0 and 65535

	//MASSIVE NUMBERS 
	long long int number7 = 123123123123123; //between -9223372036854775807 and 9223372036854775807
	unsigned long long int number8 = 1844674407370955161; // 0 to 1844674407370955161

	return 0;
}

