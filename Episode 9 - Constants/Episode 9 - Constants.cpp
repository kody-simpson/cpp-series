#include <iostream>

using namespace std;

int main() {

	//Variable Constants - variables that can't change
	//use all caps when naming constants
	const int HIGH_SCORE = 1000000;
	//cant reassign this value because its a constant
	//HIGH_SCORE = 24;

	const double PI = 3.14; //pi never changes, so make it a constant
	//then you might use it like this
	int radius = 5;
	long area = 2 * PI * radius;
	cout << area << endl;

	//Enumerations - basically variables with predefined values
	enum Names {
		Henry, //0
		Bob, //1
		Billy, //2
		Bobby, //3
		Ricky //4
	}; //it's numbered like an array

	Names name1 = Billy;
	Names name2 = Names(4); //Same as putting Ricky
	cout << name1 << endl;
	cout << name2 << endl;

	//You can also customize the numbers for the enumerators
	enum Colors {
		Red = 67,
		Blue, // 68
		Black = 2,
		Pink, //3
		Purple //4
	};

	Colors color1 = Pink;
	cout << color1 << endl;

	return 0;
}

