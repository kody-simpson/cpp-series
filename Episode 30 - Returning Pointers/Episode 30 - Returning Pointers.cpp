#include <iostream>

using namespace std;

////////////////////////////////////////////////////
// Returning Pointers in Functions
// You can return a pointer just like any variable, except you need to add a * after the type

int* retrieveAge(int amount) {

    int* age = new int;
    *age = amount;

    return age; //Return a pointer or memory address
}

////////////////////////////////////////////////////
// Make sure to never return a pointer to a local(non dynamic) variable
// If you return a pointer to a variable created on the stack,
// you are essentially using the address of something that no longer exists
// although of course the address still does.
double* getSize() {

    double sizeOfBrain = 6500;

    return &sizeOfBrain; //Bad!!! This local variable will not exist once the function call ends
}

// You can return the address of a local variable if it's passed by reference though.
// sizeOfBrain is the same address of what exists outside of the function
// so returning that as a pointer is valid, since it exists outside the function
double* getSize(double& sizeOfBrain) {

    sizeOfBrain = 6500;

    return &sizeOfBrain; //Legit
}

//Another example. Don't forget arrays are automatically passed by reference
string* chooseFavoriteName(string favoriteNames[]) {

    return &favoriteNames[1]; //Valid since favoriteNames isn't a new array. Just the one that already exists outside the function call
}

///////////////////////////////////////////////////
// One final example of a use for returning pointers

//Constructing a dynamically allocated array
string* createNamesArray(int namesCount) {

    string* names = new string[namesCount];

    for (int i = 0; i < namesCount; ++i) {
        cout << "Enter a name: ";
        cin >> names[i];
    }

    //Make sure to return the pointer to free up the memory elsewhere
    //or otherwise call the delete [] names here.
    return names;
}

int main() {

    ////////////////////////////////////////////////////////////////
    //A simple example of using a function that returns a pointer
    int* age = retrieveAge(45);
    cout << "The age is: " << *age << endl;

    //Done using age, free up the memory
    delete age;
    age = nullptr; //set to null to avoid accidentally changing anything at the old memory address

    ////////////////////////////////////////////////////////////////
    // Example of how to use the second function
    string* names = nullptr;
    int amountOfNames;

    cout << "Enter the amount of names you would like to create: ";
    cin >> amountOfNames;
    names = createNamesArray(amountOfNames);

    cout << "Here are all of the names: " << endl;
    for (int i = 0; i < amountOfNames; ++i) {
        cout << names[i] << endl;
    }

    //Don't forget to free up the memory of the names array
    delete[] names;
    names = nullptr;

    return 0;
}