#include <iostream>

using namespace std;

int main()
{

	///////////////////////////////////////////////
	//Creating an enumeration
	//
	//Enumerators are named integer constants, pure and simple.
	//By default, the first enumerator is zero, and increments from there.
	// 0, 1, 2, 3, 4, 5, 6
	enum Day { Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday };

	//Declaring a new variable of our enum
	Day favoriteDay = Friday;
	//you can also use ints
	int dayAsNumber = Tuesday; //stores 1

	//If we print out the Day variable, we can see that it does indeed represent a constant
	cout << favoriteDay << endl;
	//Prints 1
	cout << dayAsNumber << endl;

	//////////////////////////////////////////////
	//Using Enums

	//a common use of enums are to put them as switch cases
	Day birthday = Tuesday;

	cout << "My Birthday lands on a ";
	switch (birthday)
	{
	case Monday:
		cout << "Monday" << endl;
		break;
	case Tuesday:
		cout << "Tuesday" << endl;
		break;
	case Wednesday:
		cout << "Wednesday" << endl;
		break;
	case Thursday:
		cout << "Thursday" << endl;
		break;
	case Friday:
		cout << "Friday" << endl;
		break;
	case Saturday:
		cout << "Saturday" << endl;
		break;
	case Sunday:
		cout << "Sunday" << endl;
		break;
	}

	//We can create an enum with custom values for the enumerator constants
	enum TemperatureScale { Freezing = 32, Boiling = 212 };
	int temp;

	cout << "Enter the temperature of your water(F): ";
	cin >> temp;

	//Comparing enumerators
	if (temp <= Freezing) //same thing as temp <= 32
	{
		cout << "Your water is frozen!!!" << endl;
	}
	else if (temp >= Boiling) //same as temp >= 212
	{
		cout << "The water is boiling :O" << endl;
	}
	else
	{
		cout << "The water is still just a boring normal liquid";
	}

	//One of the downsides is that you cannot do math on enums
	TemperatureScale thing = Freezing;
	//thing = thing + 1;
	//thing++;


	//////////////////////////////////////////
	///More Enum Stuff
	///
	//Anonymous Enums
	//If you don't need to create a variable for your enums
	//then you can make it anonymous by not giving it a name
	enum { Red, Green, Orange, Blue };

	if (Green < Orange)
	{
		cout << "Amen sister" << endl;
	}

	//Custom values for the enums
	//As shown before, we can assign numbers to the enumerators manually
	enum Thing { Pickle = 34, Bear, Eagle, Cane };
	//From whatever value you assign to an enumerator, the next enumerator
	//will have that value plus 1, unless manually given a custom value
	//
	//This means it will look like: 34, 35, 36, 37
	cout << Pickle << endl;
	cout << Bear << endl;
	cout << Eagle << endl;
	cout << Cane << endl;

	//another example:
	// 0, 6, 7, 23, 99
	enum { Robin, Hummingbird = 6, Grouse, Sparrow = 23, Tanager = 99 };

	//If you really want, you can convert an integer into a Enum
	Day humpDay = static_cast<Day>(3);
	//You CANT do this:
	//Day humpDay = 3;

	return 0;
}