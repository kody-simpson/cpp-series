#include <iostream>

using namespace std;

int main() {

	//Multidimensional Arrays - Arrays of arrays(more than one array)
	//A regular array will extend in one direction
	//A multidimensional array will extend in multiple directions
	// [][][]
	// [][][]
	//this would be an array with 2 elements(the first and second row)
	//with each element(row) having another array inside of it with 3 more elements

	//Declare a multidimensional array
	int cards[2][3];

	//To initialize a 2d multidimensional array
	int juice[2][3] = { 2, 4, 1, 2, 3, 3 };

	//An easier to understand way
	int salamander[3][4] = {
		{4, 1, 4, 1},
		{1, 5, 2, 5},
		{2, 5, 13, 23}
	};

	//How to access or modify the values inside of a multidimensional array
	//row first, then column
	cout << "Where is the number 13? " << salamander[2][2] << endl;


	return 0;
}
