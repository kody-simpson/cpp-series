#include <iostream>

using namespace std;

//////////////////////////////////////
/// Using Structures with Functions
/// It's really the exact same as regular variables
/// that we have been using up to this point.

struct Book
{
	string title;
	string author;
	string isbn;
};

//passing a structure into a function
void displayBook(Book book)
{
	cout << "Book Info:" << endl;
	cout << book.title << " written by " << book.author << endl;
	cout << "ISBN: #" << book.isbn << endl;
}

//passing by const reference instead to help with performance 
void displayBookBetter(const Book& book)
{
	cout << "Book Info:" << endl;
	cout << book.title << " written by " << book.author << endl;
	cout << "ISBN: #" << book.isbn << endl;
}

//returning a structure from a function
Book createNewBook(string title, string author, string isbn)
{

	//create the book from the values just passed in
	Book book = { title, author, isbn };

	return book;
}

int main()
{

	//Create a book to display
	Book book = { "12 Rules for Life", "Jordan B Peterson", "0345816021" };

	//call the displayBook function and pass the book in by value
	displayBook(book);

	//call the displayBook function and pass the book in by reference
	displayBookBetter(book);

	Book goodBook = createNewBook("Unbroken", "Laura Hillenbrand", "9780812974492");

}