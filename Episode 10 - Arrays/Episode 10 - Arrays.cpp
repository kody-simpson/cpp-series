#include <iostream>

using namespace std;

int main() {

	//Arrays - a collection of elements grouped into one "variable"
	int numbers[50] = {}; //Make 50 int's all with the value of 0
	int numbers2[6] = { 12, 34, 5, 7, 4, 123 }; //6 elements with initial values
	int numbers3[7] = { 9, 2 }; //first 2 elements are initialized, rest are defaulted to 0

	//If you know all of the elements' values ahead of time, you don't need to specify a number
	int numbersArray[] = { 45, 12, 4 };

	//You can use any object/datatype
	string words[] = { "Booty", "Clappers" };

	//These are all static arrays. The length cannot be changed once initialized.
	//Arrays are zero based. The first element starts are zero
	string booksList[4] = {
		"Unbroken", //index 0
		"Can't Hurt Me", //index 1
		"The Godfather", //index 2
		"SuperMarket"  //index 3
	};

	//Now that you know how the indexing of arrays work, you can access them by their index

	//Print out the value of the THIRD book in our list
	cout << "This is the third book: " + booksList[2] << endl;

	//Changing the data in an array - use the index also
	int ages[] = { 13, 17, 12, 134, 67 };
	cout << ages[3] << endl;
	ages[3] = 135;
	cout << ages[3] << endl;

	//Another cool trick - see how much memory an array takes up using sizeof
	cout << "Size of the age array: " << (sizeof(int) * 5) << " bytes" << endl;

	return 0;
}
