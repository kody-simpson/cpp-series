#include <iostream>

using namespace std;

struct Human
{
	string name;
	int age;
	bool hasSwag;
};

void setHuman(Human* human);

int main()
{

	////////////////////////////////////////////
	///Pointing to Structures
	//Just like everything else, creating pointers to structs is the exact same
	//as regular primitive variables

	//A Human
	Human bob = { "Bob", 56, false };

	//A pointer that points to a Human
	Human* bobPtr = &bob;

	//The only thing different is how you access the members of a pointer to a structure.
	//Instead of using the dot operator, you use the structure pointer operator.
	cout << "Contents of Bob: " << endl;
	cout << bobPtr->name << endl; //The arrow operator takes care of the dereferencing automatically
	cout << bobPtr->age << endl;
	cout << bobPtr->hasSwag << endl;

	//Why use the arrow operator? Due to operator precedence, it would have to look like this
	cout << (*bobPtr).age << endl;
	//dont do this *bobPtr.age

	//Don't forget, dynamic allocation is still a thing
	Human* human1 = nullptr;
	human1 = new Human; //Dynamically allocate a Human onto the heap

	//Now, we can set the values of our Human
	human1->name = "Henry";
	human1->age = 16;
	human1->hasSwag = true;

	//And print them out
	cout << human1->name << endl;
	cout << human1->age << endl;
	cout << human1->hasSwag << endl;

	//Since we are now done using human1, we can free up it's memory
	delete human1;
	human1 = nullptr;

	//Using the setHuman function to demonstrate passing of pointers
	Human human2;
	setHuman(&human2);

	cout << human2.name << endl;
	cout << human2.age << endl;
	cout << human2.hasSwag << endl;


}

void setHuman(Human* human)
{

	cout << "Hello! Time to create a new human... easy.";
	cout << "Enter a name: ";
	cin >> human->name;
	cout << "Enter age: ";
	cin >> human->age;
	cout << "Does this person have swag? (true or false)";
	cin >> human->hasSwag;

}