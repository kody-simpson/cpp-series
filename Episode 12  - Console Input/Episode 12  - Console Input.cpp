#include <iostream>

using namespace std;

int main() {

	//Get user input and store it

	cout << "Hey, what is your age? " << endl;
	int age; //variable we will use to store their age
	cin >> age; //get input from the user and store it into the variable
	cout << "Thanks for giving me your age." << endl;
	cout << "Your age is: " << age << endl;

	//You can ask for 2 inputs in one go
	string race;
	string gender;
	cout << "Give me your race and gender." << endl;
	cin >> race >> gender;
	cout << "Thanks. Your race is then " << race << " and your gender must be a " << gender << endl;

	return 0;
}
