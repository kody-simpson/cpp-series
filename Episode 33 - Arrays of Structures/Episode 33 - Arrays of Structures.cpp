#include <iostream>

using namespace std;

//A simple structure
struct Cheeto
{
	double weight;
	int length; //value 1 through 10
	string name; //every respectable cheeto needs a name
};

int main()
{

	/////////////////////////////////////////////
	//How to make an array that can hold Cheetos
	Cheeto chipBag[3];

	//set the second cheeto of the array
	chipBag[1].weight = 20.0;
	chipBag[1].length = 5;
	chipBag[1].name = "Larry";

	//another way to set an element of our array 
	chipBag[2] = { 40.0, 2, "Jer" };

	//Giving an initialization list to an array of custom structs
	int scores[] = { 45, 23, 100 }; //typical way
	Cheeto myFriends[] = {
		{4.0, 4, "Eminem"},
		{70.0, 3, "Ken Kaniff"},
		{120.0, 7, "Marky Mark"},
		{34.0, 9, "Ike"}
	}; //Creates an array of 4 cheetos

	//Print out all of the cheetos
	cout << "All my Cheeto+ friends: " << endl;
	for (int i = 0; i < 4; i++)
	{
		cout << "Name: " << myFriends[i].name << endl;
		cout << "Weight: " << myFriends[i].weight << endl;
		cout << "Length: " << myFriends[i].length << endl;
	}

}