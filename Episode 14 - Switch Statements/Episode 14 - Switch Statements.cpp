#include <iostream>

using namespace std;

int main() {

	//Switch statements - Another way to branch code
	int number = 500; //a variable for our condition
	switch (number)
	{
	case 500: //a condition
		cout << "The number is 500 " << endl;
		break; //dont forget to put a break statement after your code
	case 200:
		cout << "The number is 200 " << endl;
		break;
	case 0:
		cout << "The number is zero" << endl;
		break;
	default: //the else condition
		cout << "I don't know that number" << endl;
		break;
	}

	//Make a cool little menu with a switch statement
	cout << "----------------" << endl;
	cout << "Food Menu:" << endl;
	cout << "1 - Taco" << endl;
	cout << "2 - Ice Cream" << endl;
	cout << "3 - Doritos" << endl;
	cout << "4 - Steak" << endl;
	cout << "----------------" << endl;
	int option;
	cout << "Enter the number for the food you want: ";
	cin >> option;

	switch (option)
	{
	case 1:
		cout << "Tacos incoming!" << endl;
		cout << "*gives tacos*" << endl;
		break;
	case 2:
		cout << "Ice cream incoming!" << endl;
		cout << "*gives ice cream*" << endl;
		break;
	case 3:
		cout << "Doritos incoming!" << endl;
		cout << "*gives doritos*" << endl;
		break;
	case 4:
		cout << "Steak incoming!" << endl;
		cout << "*gives steak*" << endl;
		break;
	default:
		cout << "You didnt't choose any of the food on the menu!" << endl;
		break;
	}

	//Use enumerations
	enum Months {
		January = 1,
		February,
		March,
		April,
		May,
		June,
		July,
		August,
		September,
		October,
		November,
		December
	};
	cout << "What is your favorite month? " << endl;
	cout << "Put the number for the month (ex. 3 for march)" << endl;
	int month;
	cin >> month;
	switch (month)
	{
	case January:
		cout << "I like January too!" << endl;
		break;
	case February:
		cout << "I like February too!" << endl;
		break;
	case March:
		cout << "I like March too!" << endl;
		break;
	case April:
		cout << "I like April too!" << endl;
		break;
	case May:
		cout << "I like May too!" << endl;
		break;
	case June:
		cout << "I like June too!" << endl;
		break;
	case July:
		cout << "I like July too!" << endl;
		break;
	case August:
		cout << "I like August too!" << endl;
		break;
	case September:
		cout << "I like September too!" << endl;
		break;
	case October:
		cout << "I like October too!" << endl;
		break;
	case November:
		cout << "I like November too!" << endl;
		break;
	case December:
		cout << "I like December too!" << endl;
		break;
	}


	return 0;
}
