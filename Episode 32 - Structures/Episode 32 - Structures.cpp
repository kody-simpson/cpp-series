#include <iostream>

using namespace std;

//Defining a structure
//This essentially creates a custom datatype 
struct Address
{
	string streetAddress; //each variable in a struct is a "member"
	string city;
	string state;
	int zipCode;
};

struct Car
{
	string make;
	string model;
	int year;
	double cost;
};

int main()
{

	//Now that we have defined what an Address is with a struct
	//, we can now create addresses like any other variable
	Address home;

	//Create multiple addresses for each one of my girlfriends
	Address girlOne, girlTwo, girlThree, girlFour;

	//////////////////////////////////////////////////////
	//Accessing structure members
	//To access one of the struct members, use the dot operator
	home.streetAddress = "600 Co rd 252";
	home.city = "Gustine";
	home.state = "TX";
	home.zipCode = 76455;

	//We can print the address if we want
	cout << "My Home Address: " << endl;
	cout << home.streetAddress << endl;
	cout << home.city << endl;
	cout << home.state << endl;
	cout << home.zipCode << endl;

	//You cannot do this
	//cout << home << endl;
	/////////////////////////////////////////////////////

	/////////////////////////////////////////////////////
	///More Examples of declaring and initializing struct variables

	Car car1, car2;

	// car1.make = "Tesla";
	// car1.model = "Model Y";
	// car1.year = 2020;
	// car1.cost = 60000;
	//
	// car2.make = "Ford";
	// car2.model = "F-150";
	// car2.year = 2020;
	// car2.cost = 28745;

	//Another way to initialize structs
	car1 = { "Tesla", "Model Y", 2020, 60000 };
	car2 = { "Ford", "F-150", 2020, 28745 };

}