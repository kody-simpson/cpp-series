#include <iostream>

using namespace std;

int main() {

	//sizeof - used to determine the size(how much memory) of datatypes/variables
	cout << "Size of int: " << sizeof(int) << endl;
	cout << "Size of long: " << sizeof(long) << endl;
	cout << "Size of short: " << sizeof(short) << endl;
	cout << "Size of double: " << sizeof(double) << endl;
	cout << "Size of boolean: " << sizeof(bool) << endl;

	//auto - used so you dont have to specify the datatype of a variable
	//especially useful for numbers and saving memory
	auto kodysIQ = 200;
	cout << sizeof(kodysIQ) << endl;

	//typedef - used to remember a datatype
	unsigned long long size = 1232132142145214611;
	typedef unsigned long long REALLY_BIG_DATATYPE;
	REALLY_BIG_DATATYPE size2 = 12321412845215115151;
	REALLY_BIG_DATATYPE size3 = 559919191919;

	return 0;
}
