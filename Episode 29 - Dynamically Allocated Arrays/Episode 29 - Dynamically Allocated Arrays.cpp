#include <iostream>

using namespace std;

int main() {

    ////////////////////////////////////////////////////////////
    // Dynamically Allocating Arrays
    // This simply means to allocate arrays on the heap rather than the stack
    // And more importantly, the size of the array does not need to be known ahead of compilation

    //How to do it
    double* scores = new double[5]; //allocates 5 doubles of memory in the heap. 40 bytes

    //give the array initial values
    scores[0] = 100.0;
    scores[1] = 94.0;
    scores[2] = 84.0;
    scores[3] = 78.0;
    scores[4] = 0.0;

    //print out the array for verification
    for (int i = 0; i < 5; ++i) {
        cout << scores[i] << endl;
    }

    //Now that we are done using the array, we need to de-allocate it
    //If you want to delete the entire array make sure you put brackets after the delete operator
    delete[] scores; //Deallocates array

    ///////////////////////////////////////////////////

    //Give the array initial values
    string* names = new string[3]{ "Bobby", "Ricky", "Johnny" }; //Dynamically allocates array with these initial string values
    for (int i = 0; i < 3; ++i) {
        cout << names[i] << endl;
    }

    ///////////////////////////////////////////////////
    // An example of how dynamically allocated arrays can benefit our immediate code

    int namesCount;
    cout << "How many names do you want to store? ";
    cin >> namesCount;

    //Using a fixed array, we cannot initialize it with the above variable. It must be a constant.
    //With dynamic arrays, it does not need to be constant. Values can be decided at runtime.
    string* favoriteNames = new string[namesCount];

    //Ask for the names
    cout << "Enter " << namesCount << " names: " << endl;
    for (int j = 0; j < namesCount; ++j) {
        cin >> favoriteNames[j];
    }

    //Print out the collected names for verification
    for (int j = 0; j < namesCount; ++j) {
        cout << favoriteNames[j] << endl;
    }

    return 0;
}