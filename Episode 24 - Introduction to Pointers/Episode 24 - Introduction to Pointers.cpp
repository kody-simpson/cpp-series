#include <iostream>

using namespace std;

int main() {

    //Pointers in C++

    //Pointers = variables that store memory addresses
    //How do we get a memory address? Put an '&' in front of a variable
    int thing = 1800;

    cout << "The value of this variable is: " << thing << endl;
    cout << "The size of this variable is: " << sizeof(thing) << endl; //prints 4 for 4 bytes
    cout << "The memory address of this variable is: " << &thing << endl; //prints memory address

    double thing2 = 17.6;
    short thing3 = 34;
    bool thing4 = false;

    //The memory address can be found for any of these variables
    cout << &thing4 << endl;

    //Now that we can find a memory address, we can store it -- in a pointer
    //Form of a pointer: <datatype of underlying variable>* <name> = <memory address>
    //How can we create a pointer that stores the memory location of a double?
    double* pointerToDouble = &thing2; //<-- pointer to double. When this statement executes, it allocated 4 bytes in the memory and stores the memory location
    //Here are some more ways you can declare it
    //double * pointerToDouble = &thing2;
    //double *pointerToDouble = &thing2;

    //Let's print this pointer so that we can see proof that it stores a memory location
    cout << "Here is a pointer to a double: " << pointerToDouble << endl;

    //Since we know the pointer points to a memory location associated with a value, we can "dereference" to get the value at that memory location
    //Dereferencing can be done with the "indirection" operator: *<variable>
    //Example:
    int bob = 17;
    int* ptrBob = &bob; //make a pointer to bob's memory location
    cout << "Value of bob: " << bob << endl;
    cout << "Memory location in pointer: " << ptrBob << endl;
    cout << "Value stored at pointer: " << *ptrBob << endl;

    //In case you did not believe pointers are variables with a size of 4 bytes
    cout << "Size of our pointer: " << sizeof(ptrBob) << endl;

    //In case you did not believe pointers are variables with their own memory address
    cout << "Memory address of pointer " << &ptrBob << endl;

    return 0;
}