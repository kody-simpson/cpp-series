#include <iostream>

using namespace std;

const int ARRAYSIZE = 5;

void doubleNumber(double* number) {
    *number = *number * 2;
}

//The address of the pointer provided as an argument is copied into the pointer for the scope of the function
void doSomething(int* pointer) {
    cout << &pointer << endl;
}

//Passing a pointer as a pointer to a constant
//Useful for when you know you want the original value not to be altered
void printName(const string* name) {
    cout << "Hello, my name is " << *name << endl;
}

//If you want to prevent the pointer's address from changing as well
//void printName(const string* const name){
//    cout << "Hello, my name is " << *name << endl;
//}

//Provide the address of the first variable in the array and double each one
void doubleArray(double* arrayPtr) {
    for (int i = 0; i < ARRAYSIZE; i++) {
        arrayPtr[i] = arrayPtr[i] * 2;
    }
}

//print the array
void printArray(const double* arrayPtr) {
    for (int i = 0; i < ARRAYSIZE; i++) {
        cout << arrayPtr[i] << endl;
    }
}

int main() {

    /////////////////////////////////////////////////////
    //Passing a pointer as a parameter

    //Why return the value when you can directly access it?
    //Therefore, you can pass it as a pointer and directly access the value from its memory address
    double number2 = 6;
    cout << "Initial Number: " << number2 << endl;
    doubleNumber(&number2); //You can pass a memory address directly or make a pointer variable and pass that
    cout << "Number after being doubled: " << number2 << endl;

    /////////////////////////////////////////////////////
    //Passing a pointer to a pointer to a constant
    const string name = "Billy bob";
    printName(&name);

    //You can even pass the address of a non-constant variable but it will still be treated as a constant in the function
    string grandma = "Grandma Simpson";
    printName(&grandma); //even though grandma is not a constant variable, the address will be copied into a pointer to one

    //////////////////////////////////////////////////////
    //Looping through arrays
    double scores[ARRAYSIZE] = { 56.7, 24.2, 131, 130.0, 100.0 };
    doubleArray(scores);
    printArray(scores);


    return 0;
}