#include <iostream>

using namespace std;

class Car
{
	//You can use the private keyword to hide member vars and functions
	//this follows the practice of Data Hiding
private:
	string make;
	string model;
	double speed;

	//It is sometimes useful to hide functions too,
	//for internal usage
	void doSomething();
public:
	//Prototypes for functions to speed up and slow down the car
	void speedUp(double increment);
	void slowDown(double decrement);

	//Accessors and Mutators for the hidden data
	//also known as getters and setters.
	//These are commonly made to provide an interface
	//to work with the hidden(private) data
	string getMake() const;
	void setMake(string newMake);
	//The const at the end of the prototype is a
	//sort of promise telling C++ that you won't be
	//modifying any member variables inside the function
	string getModel() const;
	void setModel(string newModel);
	double getSpeed() const;
};

void Car::speedUp(double increment)
{
	speed = speed + increment;
}

void Car::slowDown(double decrement)
{
	if (speed - decrement < 0)
	{
		speed = 0;
	}
	else
	{
		speed = speed - decrement;
	}
}

string Car::getMake() const
{
	return make;
}

void Car::setMake(string newMake)
{
	make = newMake;
}

string Car::getModel() const
{
	return model;
}

void Car::setModel(string newModel)
{
	model = newModel;
}

double Car::getSpeed() const
{
	return speed;
}


int main()
{

	Car mater;

	//If you try accessing one of the private members, it won't let you.
	//private members can only be accessed inside of the class
	//cout << mater.speed << endl;

	//This forces us to use the functions provided by the class instead
	mater.setMake("Booty");
	mater.setModel("Maximus");

	cout << "Here is my car: " << endl;
	cout << "Make: " << mater.getMake() << endl;
	cout << "Model: " << mater.getModel() << endl;

	//Since we have made speed private and 2 functions that change it's value,
	//the users of this class don't have access to change the speed variable
	//however they want, which is good to make sure the class is properly used.
	mater.speedUp(20.0); //speed up the car by 20 mph
	cout << "Current speed: " << mater.getSpeed() << endl;

	//Here is a good demonstration of why forcing them to use functions is good:
	//if the user had the capabilities of changing the speed variable to whatever
	//they want, they would be misusing the class. When forcing them to use the
	//slowDown function, it makes sure that if the speed goes below zero, it is
	//automatically set to zero. Without the function, that wouldn't be possible!
	mater.slowDown(34.0);
	cout << "Current speed: " << mater.getSpeed() << endl;


}
